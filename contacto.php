<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/custom.css" type="text/css" />
</head>
<body>
    <div class="form-widgetss">
        <form id="contact-form" method="POST" action="guardar_contacto.php" >
            <div class="form-container">
            <div class="form-result"></div>
                <label for="nombre">nombre</label>
                    <input type="text" name="nombre" id="nombre" class="default" data-msg="Escribe tu nombre" required/>
                    <div class="error" id="nombre-error">Escribe tu nombre</div>
            </div>
            <div class="form-container">
                <label for="email">email</label>
                    <input type="text" name="email" id="email" class="default" required/>
                    <div class="error" id="email-error">Escribe tu email</div>
            </div>
            <div class="form-container">
                <label for="telefono">telefono</label>
                    <input type="tel" name="telefono" id="telefono" class="default" required/>
                    <div class="error" id="telefono-error">Escribe tu telefono</div>
            </div>
            <div class="form-container">
                <label for="mensaje">mensaje</label>
                    <textarea name="mensaje" id="mensaje" class="default" required></textarea>
                    <div class="error" id="mensaje-error">Escribe tu mensaje</div>
            </div>
            <div class="form-container">
                    <input id="boton-enviar" type="submit" value="Enviar">
            </div>
            <input id="otrodato" type="hidden" name="otrodato"/>
        </form>
    </div>
    <script src="js/jquery.js"></script>
    <script src="js/plugins/jquery.validation.js"></script>
    
    <script src="js/validar-formulario.js"></script>
</body>
</html>