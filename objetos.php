<?php

class Carro
{
    private $velocidad;
    public $color;
    public $puertas;
    public $asientos;

    function __construct($color = "metal", $puertas = 4, $asientos = 5, $velocidad = 0) {
        $this->color = $color;
        $this->puertas = $puertas;
        $this->asientos = $asientos;
        $this->velocidad = $velocidad;
    }

    function acelerar($rapidez = 0)
    {
        if($rapidez > 0)
            $this->velocidad += $rapidez;
    }

    function frenar()
    {
        if($this->velocidad > 0)
            $this->velocidad -= 1;
    }

    function obtenerVelocidad(){
        return $this->velocidad; 
    }

}

//------puede estar en otro archivo
    $carro1 = new Carro();
    print var_dump($carro1)."<br>";
    $carro2 = new Carro("blanco", 2, 2);
    print var_dump($carro2)."<br>";
    print $carro2->obtenerVelocidad()."<br>";

    for($i=0; $i<=10; $i++){
        $carro2->acelerar(5);
        print $carro2->obtenerVelocidad()."<br>";
    }
    $carro3 = new stdClass();
    $carro3->color = "verde";
    print var_dump($carro3)."<br>";
?>