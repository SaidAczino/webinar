<div>
    <form action="include/guardar_usuario.php" method="post" enctype="multipart/form-data">
        <div>
            <label for="nombre_completo">Nombre:</label><input type="text" name="nombre_completo">
        </div>
        <div>
            <label for="email" >Correo Electrónico:</label><input type="email" name="email">
        </div>
        <div>
            <label for="telefono">Teléfono:</label><input type="tel" name="telefono">
        </div>
        <div>
            <label for="password">Contraseña:</label><input type="password" name="password">
        </div>
        <div>
            <button type="submit">Guardar</button>
        </div>
    </form>
</div>