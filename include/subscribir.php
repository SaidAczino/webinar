<?php
require_once "modelos/Usuario.php";
require_once "modelos/Evento.php";

$usuario = new Usuario;
if(!$usuario->tieneSesion()){
	$arrResult = array ('response'=>'error', 'message' => '<strong>Error</strong> Necesitas tener una sesión valida.');
    echo json_encode($arrResult);
    return false;
}

$evento_id = isset($_POST["evento"]) ? $_POST["evento"] : 0;
$evento = new Evento;
if(!$evento->obtenerEvento($evento_id)){
    $arrResult = array ('response'=>'error', 'message' => '<strong>Error</strong> Lo sentimos, no se pudo encontrar el evento.');
    echo json_encode($arrResult);
    return false;
}

//TODO: subscribir al evento
if($usuario->subscribir($evento->id)){
    $arrResult = array ('response'=>'success', 'message' => '<strong>Éxito</strong> Te has subscrito al evento.');
    echo json_encode($arrResult);
    return true;
}


$arrResult = array ('response'=>'error', 'message' => '<strong>Error</strong> Hubo un error, inténtalo de nuevo.');
echo json_encode($arrResult);
return false;
?>