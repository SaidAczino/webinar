<?php
require 'php-mailer/PHPMailerAutoload.php';

$email_to = "abdiel@abeha.mx"; $name_to="Abdiel";
$email_bcc = "said@abeha.mx";
// Check for empty fields
if(empty($_POST['email']) || !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
{
	$arrResult = array ('response'=>'error', 'message' => '<strong>Error</strong> No se recibió el correo electrónico inténtalo nuevamente.');
	echo json_encode($arrResult);
	return false;
}

if(!isset($_POST["evento"])){
	$arrResult = array ('response'=>'error', 'message' => '<strong>Error</strong> No pudimos obtener la información del evento.');
	echo json_encode($arrResult);
	return false;
} else {
	$evento_id = $_POST["evento"];
	include_once "conexion.php";
	$query = "SELECT * FROM eventos WHERE id=$evento_id";  
	$resultado = $conn->query($query);
	if($evento = $resultado->fetch_object()){
		//TODO: si existe
	}
	else{
		$arrResult = array ('response'=>'error', 'message' => '<strong>Error</strong> El evento no existe.');
		echo json_encode($arrResult);
		return false;
	}
	
}

$email_address = $_POST['email'];
$mail = new PHPMailer();

$mail->CharSet = "utf8"; // header encoding
$mail->IsSMTP(); // set mailer to use SMTP
$mail->Host = "mail.abeha.mx";  // specify main and backup server
$mail->SMTPAuth = true;     // turn on SMTP authentication
$mail->Username = "pruebas@abeha.mx";  // SMTP username
$mail->Password = "AchisAchisLosMariachis"; // SMTP password
$mail->Port = 2525;
//$mail->SMTPDebug = 2; //Alternative to above constant


// $email is the user's email address the specified
// on our contact us page. We set this variable at
// the top of this page with:
$email = $_POST['email'];
$mail->From = $email;

//Set who the message is to be sent from
$mail->setFrom('pruebas@abeha.mx', 'Suscripción a '.$evento->nombre);

// below we want to set the email address we will be sending our email to.
$mail->AddAddress($email_to, $name_to);

// below we want to set the blind carbon copy (BCC) email address we will be sending our email to.
if(isset($email_bcc) && $email_bcc != "")
$mail->AddBCC($email_bcc, "Copia");

// set word wrap to 50 characters
$mail->WordWrap = 50;
// set email format to HTML
$mail->IsHTML(true);

$mail->Subject = "$email se ha agregado al boletín.";

$mail->Body    = "<b>$email_address</b> Se ha suscrito al boletín.";
$mail->AltBody = "$email_address Se ha suscrito al boletín.";

if(!$mail->Send())
{
	$arrResult = array ('response'=>'error', "message" => "No se pudo enviar el correo electrónico, inténtalo nuevamente", "err"=>json_encode($mail->ErrorInfo));
	echo json_encode($arrResult);
}
else {
	$arrResult = array ('response'=>'success', 'message' => 'Gracias por subscribirte, recibirás un correo electrónico de confirmación.');
	echo json_encode($arrResult);
}

?>
