<?php 
include_once "../include/conexion.php";
//Se obtiene todos los parámetros que vienen en la url.
header('Content-Type: application/json');

$data  =  $_POST;
$evento_id = isset($data["evento"]) ? $data["evento"] : 0;

if(!$evento_id){
    http_response_code(409);
    echo json_encode(
        [
            "mensaje" => "Parámetros incorrectos"
        ]
        );
        return;  
} 
    
//se valida si existe el vento
$query = "SELECT * FROM eventos WHERE id=$evento_id";
$resultado = $conn->query($query);
if($evento = $resultado->fetch_object()){
    //si el evento existe, se borra
    $query = "DELETE FROM eventos WHERE id=$evento_id";
    $resultado = $conn->query($query);
    
    if($resultado){
        http_response_code(200);
        echo json_encode(
            [
                "mensaje" => "Se ha borrado el evento exitosamente"
            ]
        );
        return; 
    } else {
        http_response_code(409);
        echo json_encode(
            [
                "mensaje" => "No se pudo borrar el evento. ".mysqli_error($conn)
            ]
        );
        return; 
    }
}
else {
    http_response_code(404);
    echo json_encode(
    [
        "mensaje" => "No existe el evento"
        ]
    );
    return;
}
        
        
        
        ?>