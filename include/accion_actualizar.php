<?php 
    require_once "modelos/Evento.php";
    $data  =  $_POST;
    echo var_dump($data);

    $nombre = $_POST["nombre"];
    $descripcion = $_POST["descripcion"];
    $fecha = $_POST["fecha"];
    $tiempo = $_POST["tiempo"];
    $evento_id = $_POST["evento"];
    $directorio = "../images/uploads/";

    if(!$evento_id) die("No existe el id del evento");
    $evento = new Evento;
    $existe = $evento->obtenerEvento($evento_id);
    if($existe)
        echo "<div>Evento <b>$evento->nombre</b></div>";
    else   
        die("No existe el evento");

    if(isset($_FILES["imagen"]) && $_FILES["imagen"]["size"] > 0){
        $archivo_a_guardar = $directorio . basename($_FILES["imagen"]["name"]);
        $nombre_imagen = $_FILES["imagen"]["name"];
        $uploadOk = 1;

        $imageFileType = strtolower(pathinfo($archivo_a_guardar,PATHINFO_EXTENSION));

        if (file_exists($archivo_a_guardar)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }

        if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $archivo_a_guardar)) {
            echo "The file ". basename( $_FILES["imagen"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    } else {
        //TODO: add the same name
        $nombre_imagen = $evento->imagen;
    }

    if($evento->actualizar($nombre, $nombre_imagen, addslashes($descripcion), $fecha, $tiempo)){
        echo "<div>Se actualizó el evento</div>";
    } else {
        echo "<div>No se pudo actualizar el evento</div>";
        echo mysqli_error($conn);
    }
?>