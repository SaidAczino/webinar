<?php
    date_default_timezone_set("America/Mexico_City");
	header('Content-Type: application/json');

    $datos = $_GET;
    if(isset($datos["accion"])){
        include_once "../include/conexion.php";
        if(!$conn){
            http_response_code(500);
            echo json_encode(["mensaje" => "No se pudo establecer una conexión con la base"]);
        } 

        switch($datos["accion"]){
            case "eventos":
                $fecha = date("Y-m-d H:i:s", time());
                $query = "SELECT * FROM eventos WHERE fecha > '$fecha'";
                $result = $conn->query($query);
                $eventos = [];
                while($row = $result->fetch_object()){
                    $eventos[] = $row; //$row es un evento
                }
                echo json_encode($eventos);
                return;
            break;
            default;
            break;
        }
        return;
    }

    http_response_code(404);
    echo json_encode(["mensaje" => "No se pudo encontrar la acción especificada"]);
?>