<?php
include_once "Modelo.php"; 
class Contacto extends Modelo{
    
    public $id;
    public $nombre;
    public $email;
    public $telefono;
    public $mensaje;

    /**
     * inicialización del Contacto
     */
    function __construct($nombre="", $email="", $telefono="", $mensaje=""){
        $this->nombre = $nombre;
        $this->email = $email;
        $this->telefono = $telefono;
        $this->mensaje = $mensaje;
    }
    
    function guardar(){
        $this->conectar();
        $sql = "INSERT INTO form (Nombre, Email, Telefono, Mensaje) VALUES ('$this->nombre', '$this->email', '$this->telefono', '$this->mensaje')";
        $result = $this->conexion->query($sql);
        $this->id = $this->conexion->insert_id;
        return $this->id;
    }

    function esValido(){
        return $this->nombre && $this->email && $this->telefono && $this->mensaje;
    }
}

?>