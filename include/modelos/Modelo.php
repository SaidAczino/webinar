<?php

class Modelo{
    public $conexion;
    private $host="localhost";
    private $port=3306;
    private $socket="";
    private $user="webinar_user";
    private $password="webinar_password";
    private $dbname="webinars";

    function conectar(){
        $this->conexion = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket)
            or die ('Could not connect to the database server' . mysqli_connect_error());
    }
}

?>