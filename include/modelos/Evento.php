<?php
include_once "Modelo.php"; 
class Evento extends Modelo{
    public $id;
    public $nombre;
    public $descripcion;
    public $imagen;
    public $fecha;

    /**
     * inicialización del Evento
     */
    function __construct($nombre ="", $descripcion="", $imagen="", $fecha=""){
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->imagen = $imagen;
        $this->fecha = $fecha;
    }

    function guardar(){
        $this->conectar();
        $sql = "INSERT INTO eventos (nombre, descripcion, imagen, fecha) VALUES ('$this->nombre', '$this->descripcion', '$this->imagen', '$this->fecha')";
        $result = $this->conexion->query($sql);
        $this->id = $this->conexion->insert_id;
        return $this->id;
    }

    function actualizar($nombre, $nombre_imagen, $descripcion, $fecha, $tiempo){
        $this->conectar();
        $sql = "UPDATE eventos SET nombre='$nombre', imagen='$nombre_imagen', descripcion='$descripcion', fecha='$fecha $tiempo' WHERE id=$this->id";
        $result = $this->conexion->query($sql);
        return $result;
    }


    function esValido(){
        return $this->nombre && $this->descripcion && $this->fecha;
    }

    function obtenerEvento($id){
        $this->conectar();
        $sql = "SELECT * FROM eventos WHERE id=$id";
        $result = $this->conexion->query($sql);
        if($result){
            $evento = mysqli_fetch_object($result);
            if($evento){
                $this->id = $evento->id;
                $this->nombre = $evento->nombre;
                $this->descripcion = $evento->descripcion;
                $this->imagen = $evento->imagen;
                $this->fecha = $evento->fecha;
                return true;
            }
            return false;
        }
        return false;
    }

    
}
?>