<?php
include_once "Modelo.php"; 
include_once "Subscripcion.php"; 
class Usuario extends Modelo{
    
    public $id;
    public $nombre_completo;
    public $email;
    public $telefono;
    public $password;
    public $eventos = [];

    /**
     * inicialización del Usuario
     */
    function __construct($nombre_completo="", $email="", $telefono="", $password=""){
        $this->nombre = $nombre_completo;
        $this->email = $email;
        $this->telefono = $telefono;
        $this->password = $password;
    }
    
    function guardar(){
        $this->conectar();
        $password = sha1($this->password);
        $sql = "INSERT INTO usuarios (nombre_completo, email, telefono, password) VALUES ('$this->nombre_completo', '$this->email', '$this->telefono', '$password')";
        $result = $this->conexion->query($sql);
        $this->id = $this->conexion->insert_id;
        return $this->id;
    }

    function esValido(){
        return $this->nombre_completo && $this->email && $this->telefono && $this->password;
    }

    function login(){
        $this->conectar();
        $password = sha1($this->password);
        $sql = "SELECT * FROM usuarios WHERE email='$this->email' and password='$password'";
        $result = $this->conexion->query($sql);
        if($result){
            $usuario = mysqli_fetch_object($result);
            if($usuario){
                $this->id = $usuario->id;
                $this->nombre_completo = $usuario->nombre_completo;
                $this->telefono = $usuario->nombre_completo;
                $this->password = $usuario->password;
                return true;
            }
            return false;
        }
        return false;
    }

    function obtenerUsuario($id){
        $this->conectar();
        $sql = "SELECT * FROM usuarios WHERE id=$id";
        $result = $this->conexion->query($sql);
        if($result){
            $usuario = mysqli_fetch_object($result);
            if($usuario){
                $this->id = $usuario->id;
                $this->nombre_completo = $usuario->nombre_completo;
                $this->telefono = $usuario->telefono;
                $this->email = $usuario->email;
                $this->password = $usuario->password;
                return true;
            }
            return false;
        }
        return false;
    }

    function tieneSesion(){
        session_start();
        if(isset($_SESSION["user_id"])){
            $this->obtenerUsuario($_SESSION["user_id"]);
            if($this->id){
                $this->session_id = session_id();
                return true;
            }
        }

        return false;
    }

    function subscribir($evento){
        $suscripcion = new Subscripcion($this->id, $evento);
        return $suscripcion->guardar();
    }

    function obtenerEventos(){
        $subscripcion = new Subscripcion;
        $this->eventos = $subscripcion->obtenerEventosPorUsuario($this->id);
    }
}

?>