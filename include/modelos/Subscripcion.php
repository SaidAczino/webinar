<?php
include_once "Modelo.php"; 
class Subscripcion extends Modelo{
    public $id;
    public $usuario;
    public $evento;
    function __construct($usuario, $evento){
        $this->usuario = $usuario;
        $this->evento = $evento;
    }
    
    function guardar (){
        $this->conectar();
        $sql = "INSERT INTO subscripciones (usuario_id, evento_id) VALUES ($this->usuario, $this->evento)";
        $result = $this->conexion->query($sql);
        $this->id = $this->conexion->insert_id;
        return $this->id;
    }

    function obtenerEventosPorUsuario($usuario){
        $this->conectar();
        $sql = "SELECT * from susbscripciones where usuario_id=$usuario";
        $result = $this->conexion->query($sql);
        $this->id = $this->conexion->insert_id;
        return $this->id;
    }
}