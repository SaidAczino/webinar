
$(document).ready(formValidate);


function formValidate(){
    $("#form-contacto").on("submit", function(event){
        error = false;
        $nombre = $("#nombre");
        $email = $("#email");
        $telefono = $("#telefono");
        $mensaje = $("#mensaje");

        clearErrors();

        if($nombre.val() == ""){
            $nombre.addClass("error");
            $("#nombre-error").html("Escribe tu nombre");
            showError("nombre");
            $nombre.focus();
            error = true;
        }
    
        if($nombre.val() && $nombre.val().length < 3){
            $nombre.addClass("error");
            $("#nombre-error").html("Escribe un nombre válido");
            showError("nombre");
            $nombre.focus();
            error = true;
        }
        if($email.val() == ""){
            $email.addClass("error");
            showError("email");
            if(!error)
                $email.focus();
            error = true;
        }

        if($telefono.val() == ""){
            $telefono.addClass("error");
            showError("telefono");
            if(!error)
                $telefono.focus();
            error = true;
        }
        
        if($mensaje.val() == ""){
            $mensaje.addClass("error");
            showError("mensaje");
            if(!error)
                $mensaje.focus();
            error = true;
        }
        return !error;

    });

    function showError(id){
        $(`#${id}-error`).show();
    }

    function clearErrors(){
        $("div.error").hide();
        $("#nombre").removeClass("error");
        $("#email").removeClass("error");
        $("#telefono").removeClass("error");
        $("#mensaje").removeClass("error");
    }

    //validationjs jquery plugin
    $("#contact-form").validate({
        submitHandler: function(form) {
            var $form = $(form);
            
            $.ajax({
                url: $form.attr("action"),
                method: $form.attr("method"),
                data: {
                    nombre: $form.find("input[name='nombre']").val(),
                    email: $form.find("input[name='email']").val(),
                    telefono: $form.find("input[name='telefono']").val(),
                    mensaje: $form.find("textarea[name='mensaje']").val(),
                    otrodato: "lo que sea",
                    dato3: "bla bla"
                },
                dataType: "json",
                success: function(data){
                    console.log("success",data); $form[0].reset();
                    alert(data.message);
                },
                error: function(error){ 
                    alert(error.responseJSON.message);
                },
                beforeSend: function(){ $("#boton-enviar").attr("disabled","disabled") },
                complete: function(){ $("#boton-enviar").removeAttr("disabled") }
            });
        }
    });
}