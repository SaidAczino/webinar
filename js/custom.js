
$(document).ready(function () {
    $.ajax(
        {
            url: "include/eventos.php",
            method: "get",
            dataType: "json",
            success: onSuccess,
            error: onError,
            beforeSend: onBeforeSend,
            complete: onComplete
        }
        );
        
        function onSuccess(datos){
            console.log("Se realizó la peticion correctamente", datos);
            var eventos_texto = "";
            for(i=0; i<datos.length; i++){
                eventos_texto += crearTarjetaDeEvento(datos[i]);
            }
            if(eventos_texto==="") eventos_texto = "<b>No existe ningun evento</b>"
            $("#posts").html(eventos_texto);
            
            creaEventosEnCalendario(datos);
        }
        
        function crearTarjetaDeEvento(evento){
            var imagen = "images/uploads/"+ evento.imagen;
            var fecha = new Date(evento.fecha);
            var hora = fecha.getHours()+":"+fecha.getMinutes();
            return '<div class="entry clearfix">' +
            '<div class="entry-image d-md-none d-lg-block">' +
            '<a href="#">' +
            '<img src="'+imagen+'" alt="'+evento.nombre+'">' +
            '<div class="entry-date">'+fecha.getDate()+'<span>'+(fecha.getMonth()+1)+'</span></div>' +
            '</a>' +
            '</div>' +
            '<div class="entry-c">' +
            '<div class="entry-title">' +
            '	<h2><a href="#">'+evento.nombre+'</a></h2>' +
            '</div>' +
            '<ul class="entry-meta clearfix">' +
            '	<li><span class="badge badge-warning">Privado</span></li>' +
            '	<li><a href="#"><i class="icon-time"></i>'+hora+'</a></li>' +
            '	<li><a href="#"><i class="icon-map-marker2"></i> México</a></li>' +
            '</ul>' +
            '<div class="entry-content">' +
            '	<a href="detalle_evento.php?id_evento='+evento.id+'" id="ir_curso-'+evento.id+'" class="btn btn-secondary" >Mostrar Evento</a>' +
            '	<button class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-lg-'+evento.id+'">Información</button>' +
            '	<div class="modal fade bs-example-modal-lg-'+evento.id+'" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">' +
            '		<div class="modal-dialog modal-lg">' +
            '			<div class="modal-body">' +
            '				<div class="modal-content">' +
            '					<div class="modal-header">' +
            '						<h4 class="modal-title" id="myModalLabel">'+evento.nombre+'</h4>' +
            '						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '					</div>' +
            '					<div class="modal-body">' +
            '						<p class="nobottommargin">'+evento.descripcion+'</p>' +
            '					</div>' +
            '				</div>' +
            '			</div>' +
            '		</div>' +
            '	</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        }
        
        function onError(error){
            console.log("Hubo un error", error);
            
            switch(error.status){
                case 404:
                alert("No se pudo encontrar el servicio de eventos")
                break;
                case 500:
                alert("Hubo un error de servidor");
                break;    
                default:
                break;
            }
        }
        function onBeforeSend(){ 
            $(".procesando").show(1000);
        }
        function onComplete(){
            $(".procesando").hide(1000);
        }
        
        //calendario --------------
        function creaEventosEnCalendario(eventos){
            var eventosCanvas = {};
            
            for(i=0; i<eventos.length; i++){
                var evento = eventos[i];
                var fecha = new Date(evento.fecha);
                var mes = (fecha.getMonth()+1) >= 10 ? fecha.getMonth()+1 : `0${fecha.getMonth()+1}`;
                var llave = `${mes}-${fecha.getDate()}-${fecha.getFullYear()}`;
                var hora = fecha.getHours()+":"+fecha.getMinutes();
                eventosCanvas[llave] = {startTime: hora, endTime: "", content: `<a href="detalle_evento.php?evento=${evento.id}" target=_blank>${evento.nombre}</a>`};
            }
            //copiado del template
            var cal = $( '#calendar' ).calendario( {
                onDayClick : function( $el, $contentEl, dateProperties ) {
                    for( var key in dateProperties ) {
                        console.log( key + ' = ' + dateProperties[ key ] );
                    }
                    
                },
                caldata : eventosCanvas
            }),
            $month = $( '#calendar-month' ).html( cal.getMonthName() ),
            $year = $( '#calendar-year' ).html( cal.getYear() );
            
            $( '#calendar-next' ).on( 'click', function() {
                cal.gotoNextMonth( updateMonthYear );
            } );
            $( '#calendar-prev' ).on( 'click', function() {
                cal.gotoPreviousMonth( updateMonthYear );
            } );
            $( '#calendar-current' ).on( 'click', function() {
                cal.gotoNow( updateMonthYear );
            } );     
            function updateMonthYear() {
                $month.html( cal.getMonthName() );
                $year.html( cal.getYear() );
            }
        }
        //fin calendario --------------

    });
    
    function multiplicacion(a, b){
        return a*b;
    }
    
    
    function validateInteger(variable){
        if (typeof(variable) === "number" && variable % 1 === 0)
        return true;
        else
        return false;
    }
    
    
    
    