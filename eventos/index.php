<?php
    include_once "../include/conexion.php";
    $query = "Select * from eventos";  
    $resultado = $conn->query($query);
?>
<link rel="stylesheet" href="../css/admin.css" type="text/css" />
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<table>
    <tr>
        <th>Nombre</th><th>Descripción</th><th>Fecha</th><th>imagen</th><th>Acciones</th>
    </tr>
    <?php
        while($evento = $resultado->fetch_object()){
            $fecha = new DateTime($evento->fecha);
			echo "<tr id='evento-$evento->id'>
                    <td>$evento->nombre</td><td>$evento->descripcion</td><td>".$fecha->format("d-m-Y H:i:s")."</td><td><div class='img-div' style='background-image: url(../images/uploads/$evento->imagen)'/> </td>
                    <td>
                        <button onclick='window.open(\"actualizar.php?evento=$evento->id\")'>Actualizar</button> 
                        <button 
                            data-nombre='$evento->nombre' 
                            data-evento_id='$evento->id' 
                            data-border='1px solid red'
                            class='boton-borrar'>
                                Borrar
                        </button>
                        <img class='loader' id='loader-$evento->id' src='../images/loader_ajax.gif'>
                    </td>
                </tr>";
        }
    ?>        
</table>

<script>
    $(document).ready(function(){
        $('.boton-borrar').each(function(i){
            var datos = $(this).data();
            $(this).css("border", datos.border);
        });

        $('.boton-borrar').click(function(e){
            e.preventDefault();
            var el = $(this);
            var datos = el.data();
            el.css("font-size", datos.fontsize)
            var respuesta = confirm(`¿Seguro que quieres borrar el elemento ${datos.nombre}?`);
            if(respuesta){
                $.ajax({
                    url: `../include/accion_borrar.php`,
                    dataType: "json",
                    data: {
                        evento: datos.evento_id
                    },
                    method: "POST",
                    success: function(res){
                        el.fadeOut(1000, function(){
                            $("#evento-"+datos.evento_id).remove();
                            if(res.mensaje)
                                alert(res.mensaje);
                            else    
                                alert("Se ha eliminado el evento "+datos.nombre);
                        });

                    },
                    error: function(error){
                        console.log(error);
                        if(error.responseJSON && error.responseJSON.mensaje)
                            alert(error.responseJSON.mensaje);
                        else    
                            alert("Hubo un error inténtalo de nuevo");
                        
                        $("#loader-"+datos.evento_id).fadeOut(100,function(){
                            el.fadeIn(100);
                        });
                    },
                    beforeSend: function(){
                        el.fadeOut(100,function(){
                            $("#loader-"+datos.evento_id).fadeIn(100);
                        });
                    }
                });
            }
                
        });
    })
</script>