<?php
    $evento_id = isset($_GET["evento"]) ? $_GET["evento"] : null;
    if(!$evento_id) die("No existe el id del evento");
    include_once "../include/conexion.php";
    $query = "SELECT * from Eventos where id=$evento_id";  
    $resultado = $conn->query($query);
    $evento = $resultado->fetch_object();
    if($evento)
        echo "<div>Evento <b>$evento->nombre</b></div>";
    else   
        die("No existe el evento");

    //si exsite    
    $fecha = new DateTime($evento->fecha);   
?>
<body>

    <div>
        <?=$fecha->format("d/m/Y")?>
        <form action="../include/accion_actualizar.php" method="POST" enctype="multipart/form-data">
            <label for="nombre">Nombre:</label><input type="text" name="nombre" value="<?=$evento->nombre?>">
            <label for="descripcion" >descripcion:</label><input type="text" name="descripcion" value="<?=$evento->descripcion?>">
            <label for="imagen">Imagen:</label><input type="file" name="imagen">
            <label for="fecha">fecha:</label><input type="date" name="fecha" value="<?=$fecha->format("Y-m-d")?>">
            <label for="tiempo">hora:</label><input type="time" name="tiempo" value="<?=$fecha->format("h:i:s")?>">
            <input type="hidden" value="<?=$evento->id?>" name="evento">
            <button type="submit">Actualizar</button>
        </form>
    </div>

    <script src="../js/jquery.js"></script>
    <script src="../js/actualizar.js"></script>

</body>