<?php
$eventos = [];
require_once "include/modelos/Usuario.php";
$usuario = new Usuario;
if(!$usuario->tieneSesion()){
	$login_url = "login.php";
	$login_text = "Inicia sesión";
}
else{
	$login_url = "logout.php";
	$login_text = "Cerrar sesión";
}

?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="css/calendar.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Home - Events Layout | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.html" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.html" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul>
							<li class="current"><a href="index.html"><div>Home</div></a></li>
							<li><a href="#"><div>Features</div></a></li>
							<li><a href="#"><div>Pages</div></a></li>
							<li><a href="<?=$login_url?>"><div><?=$login_text?></div></a></li>
						</ul>

					

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<section id="slider" class="slider-element slider-parallax full-screen dark" style="overflow: hidden; background: url('images/events/parallax/home.jpg') no-repeat center center;background-size: cover;">

			<div class="slider-parallax-inner">

				<div class="container clearfix vertical-middle" style="z-index: 3;">

					<div class="heading-block title-center nobottomborder">
						<h1>Accede al siguiente curso de <span id="mostrar_curso"></span></h1>
					</div>

					<div id="countdown-ex1" class="countdown countdown-large coming-soon divcenter bottommargin" style="max-width:700px;"></div>

					<div class="center topmargin-lg">

						<a href="#" id="comprar" class="button button-3d button-purple button-rounded button-xlarge">Comprar Entrada</a>
						<span class="d-none d-md-inline-block"> - O - </span>
						<a href="#" id="info" class="button button-3d button-white button-light button-rounded button-xlarge" >Información</a>
						

					</div>

				</div>

			</div>

		</section>

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="col_one_fourth nobottommargin">
						<div class="feature-box fbox-effect fbox-center fbox-outline fbox-dark nobottomborder">
							<div class="fbox-icon">
								<a href="#"><i class="icon-calendar i-alt"></i></a>
							</div>
							<h3>Sesiones Interactivas<span class="subtitle">Lorem ipsum dolor sit</span></h3>
						</div>
					</div>

					<div class="col_one_fourth nobottommargin">
						<div class="feature-box fbox-effect fbox-center fbox-outline fbox-dark nobottomborder">
							<div class="fbox-icon">
								<a href="#"><i class="icon-map i-alt"></i></a>
							</div>
							<h3>Excelentes locaciones <span class="subtitle">Officia ipsam laudantium</span></h3>
						</div>
					</div>

					<div class="col_one_fourth nobottommargin">
						<div class="feature-box fbox-effect fbox-center fbox-outline fbox-dark nobottomborder">
							<div class="fbox-icon">
								<a href="#"><i class="icon-microphone2 i-alt"></i></a>
							</div>
							<h3>Global Speakers<span class="subtitle">Laudantium cum dignissimos</span></h3>
						</div>
					</div>

					<div class="col_one_fourth nobottommargin col_last">
						<div class="feature-box fbox-effect fbox-center fbox-outline fbox-dark nobottomborder">
							<div class="fbox-icon">
								<a href="#"><i class="icon-food2 i-alt"></i></a>
							</div>
							<h3>Grandes comensales<span class="subtitle">Perferendis accusantium quae</span></h3>
						</div>
					</div>

					<div class="clear"></div>

					<div class="divider divider-short divider-center"><i class="icon-circle-blank"></i></div>

					<div class="heading-block center">
						<h2>Presentadores.</h2>
						<span>Over 15 Speakers &amp; 1000+ Developers in attendance.</span>
					</div>

					<div class="col_one_third nobottommargin">

						<div class="team">
							<div class="team-image">
								<img src="images/team/3.jpg" alt="John Doe">
							</div>
							<div class="team-desc">
								<div class="team-title"><h4>John Doe</h4><span>/ CEO, <small>Apple Inc.</small></span></div>
								<div class="team-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat assumenda similique unde mollitia eum rerum at voluptatibus saepe totam possimus.</div>
							</div>
						</div>

					</div>

					<div class="col_one_third nobottommargin">

						<div class="team">
							<div class="team-image">
								<img src="images/team/2.jpg" alt="Josh Thompson">
							</div>
							<div class="team-desc">
								<div class="team-title"><h4>Josh Thompson</h4><span>/ Co-Founder, <small>Google Inc.</small></span></div>
								<div class="team-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat assumenda similique unde mollitia eum rerum at voluptatibus saepe totam possimus.</div>
							</div>
						</div>

					</div>

					<div class="col_one_third nobottommargin col_last">

						<div class="team">
							<div class="team-image">
								<img src="images/team/8.jpg" alt="Mary Jane">
							</div>
							<div class="team-desc">
								<div class="team-title"><h4>Mary Jane</h4><span>/ Entreprenuer</span></div>
								<div class="team-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat assumenda similique unde mollitia eum rerum at voluptatibus saepe totam possimus.</div>
							</div>
						</div>

					</div>

					<div class="clear"></div>

				</div>

				<div class="section nobottommargin">
					<div class="container clearfix">

						<div class="heading-block center">
							<h3>Receive <span>regular</span> Event Updates</h3>
						</div>

						<div class="subscribe-widget">
							<div class="widget-subscribe-form-result"></div>
							<form id="widget-subscribe-form2" action="include/subscribe.php" method="post" class="nobottommargin">
								<div class="input-group input-group-lg divcenter" style="max-width:600px;">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="icon-email2"></i></div>
									</div>
									<input type="email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
									<div class="input-group-append">
										<button class="btn btn-secondary" id="subscribe" type="submit">Subscríbete</button>
									</div>
								</div>
							</form>
						</div>

					</div>
				</div>

				<div class="parallax bottommargin-lg dark" style="padding: 60px 0; background-image: url('images/parallax/calendar.jpg'); background-repeat: repeat-y; height: auto;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -500px;">

					<div class="container clearfix">

						<div class="events-calendar">
							<div class="events-calendar-header clearfix">
								<h2>Events Calendar</h2>
								<h3 class="calendar-month-year">
									<span id="calendar-month" class="calendar-month"></span>
									<span id="calendar-year" class="calendar-year"></span>
									<nav>
										<span id="calendar-prev" class="calendar-prev"><i class="icon-chevron-left"></i></span>
										<span id="calendar-next" class="calendar-next"><i class="icon-chevron-right"></i></span>
										<span id="calendar-current" class="calendar-current" title="Got to current date"><i class="icon-reload"></i></span>
									</nav>
								</h3>
							</div>
							<div id="calendar" class="fc-calendar-container"></div>
						</div>


					</div>

				</div>

				<div class="container clearfix">

					<div class="col_three_fifth bothsidebar nobottommargin">

						<div class="fancy-title title-border">
							<h3>Próximos eventos</h3>
							<div class="procesando">Cargando eventos...</div>
						</div>
						<div id="posts" class="events small-thumbs">
							<?php
							
							for($i=0 ; $i < count($eventos); $i++ ) :
								$evento = $eventos[$i];
								$fecha = new DateTime($evento->fecha);
								$dia = $fecha->format('d');
								$mes = $fecha->format('m');
								$hora = $fecha->format("H:i");
								$imagen = "images/uploads/".$evento->imagen;
							?>
							<div class="entry clearfix">
								<div class="entry-image d-md-none d-lg-block">
									<a href="#">
										<img src="<?=$imagen?>" alt="Inventore voluptates velit totam ipsa tenetur">
										<div class="entry-date"><?=$dia?><span><?=$mes?></span></div>
									</a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h2><a href="#"><?=$evento->nombre?></a></h2>
									</div>
									<ul class="entry-meta clearfix">
										<li><span class="badge badge-warning">Privado</span></li>
										<li><a href="#"><i class="icon-time"></i> <?=$hora?></a></li>
										<li><a href="#"><i class="icon-map-marker2"></i> México</a></li>
									</ul>
									<div class="entry-content">
										<a href="detalle_evento.php?id_evento=<?=$evento->id?>" id="ir_curso" class="btn btn-secondary" >Mostrar Evento</a> 
										
										<!-- Button trigger modal -->
										<!-- Large modal -->
										<button class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-lg-<?=$evento->id?>">Información</button>

										<div class="modal fade bs-example-modal-lg-<?=$evento->id?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-body">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title" id="myModalLabel"><?=$evento->nombre?></h4>
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														</div>
														<div class="modal-body">
															<p class="nobottommargin"><?=$evento->descripcion?></p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
						   	</div>
							<?php 
							endfor;
							?>
						</div>

					</div>

					<div class="col_two_fifth nobottommargin col_last">


						<div class="fancy-title title-border">
							<h4>Gallery</h4>
						</div>

						<div class="col_full masonry-thumbs grid-4 clearfix" data-lightbox="gallery">
							<a href="images/events/1.jpg" data-lightbox="gallery-item"><img class="image_fade" src="images/webinar1.jpg" alt="Gallery Thumb 1"></a>
							<a href="images/events/2.jpg" data-lightbox="gallery-item"><img class="image_fade" src="images/webinar2.jpg" alt="Gallery Thumb 2"></a>
							<a href="images/events/3.jpg" data-lightbox="gallery-item"><img class="image_fade" src="images/webinar1.jpg" alt="Gallery Thumb 3"></a>
							<a href="images/events/4.jpg" data-lightbox="gallery-item"><img class="image_fade" src="images/webinar2.jpg" alt="Gallery Thumb 4"></a>
						</div>

						<div class="fancy-title title-border">
							<h4>Video</h4>
						</div>

						<iframe width="560" height="315" src="https://www.youtube.com/embed/wooEOhp9cFY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

					</div>

					<div class="clear"></div>


				</div>

				<div class="section footer-stick notopmargin">

					<h4 class="uppercase center">Comentarios de los<span>Asistentes</span></h4>

					<div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">
						<div class="flexslider">
							<div class="slider-wrap">
								<div class="slide">
									<div class="testi-image">
										<a href="#"><img src="images/testimonials/3.jpg" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
										<div class="testi-meta">
											Steve Jobs
											<span>Apple Inc.</span>
										</div>
									</div>
								</div>
								<div class="slide">
									<div class="testi-image">
										<a href="#"><img src="images/testimonials/2.jpg" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
										<div class="testi-meta">
											Collis Ta'eed
											<span>Envato Inc.</span>
										</div>
									</div>
								</div>
								<div class="slide">
									<div class="testi-image">
										<a href="#"><img src="images/testimonials/1.jpg" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
										<div class="testi-meta">
											John Doe
											<span>XYZ Inc.</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="col_two_third">

						<div class="col_one_third">

							<div class="widget clearfix">

								<img src="images/footer-widget-logo.png" alt="" class="footer-logo">

								<p>We believe in <strong>Simple</strong>, <strong>Creative</strong> &amp; <strong>Flexible</strong> Design Standards.</p>

								<div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>Headquarters:</strong><br>
										795 Folsom Ave, Suite 600<br>
										San Francisco, CA 94107<br>
									</address>
									<abbr title="Phone Number"><strong>Phone:</strong></abbr> (91) 8547 632521<br>
									<abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br>
									<abbr title="Email Address"><strong>Email:</strong></abbr> info@canvas.com
								</div>

							</div>

						</div>

						<div class="col_one_third">

							<div class="widget widget_links clearfix">

								<h4>Blogroll</h4>

								<ul>
									<li><a href="http://codex.wordpress.org/">Documentation</a></li>
									<li><a href="http://wordpress.org/support/forum/requests-and-feedback">Feedback</a></li>
									<li><a href="http://wordpress.org/extend/plugins/">Plugins</a></li>
									<li><a href="http://wordpress.org/support/">Support Forums</a></li>
									<li><a href="http://wordpress.org/extend/themes/">Themes</a></li>
									<li><a href="http://wordpress.org/news/">WordPress Blog</a></li>
									<li><a href="http://planet.wordpress.org/">WordPress Planet</a></li>
								</ul>

							</div>

						</div>

						<div class="col_one_third col_last">

							<div class="widget clearfix">
								<h4>Recent Posts</h4>

								<div id="post-list-footer">
									<div class="spost clearfix">
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
											</div>
											<ul class="entry-meta">
												<li>10th July 2014</li>
											</ul>
										</div>
									</div>

									<div class="spost clearfix">
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
											</div>
											<ul class="entry-meta">
												<li>10th July 2014</li>
											</ul>
										</div>
									</div>

									<div class="spost clearfix">
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
											</div>
											<ul class="entry-meta">
												<li>10th July 2014</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>

					<div class="col_one_third col_last">

						<div class="widget clearfix" style="margin-bottom: -20px;">

							<div class="row">

								<div class="col-lg-6 bottommargin-sm">
									<div class="counter counter-small"><span data-from="50" data-to="15065421" data-refresh-interval="80" data-speed="3000" data-comma="true"></span></div>
									<h5 class="nobottommargin">Total Downloads</h5>
								</div>

								<div class="col-lg-6 bottommargin-sm">
									<div class="counter counter-small"><span data-from="100" data-to="18465" data-refresh-interval="50" data-speed="2000" data-comma="true"></span></div>
									<h5 class="nobottommargin">Clients</h5>
								</div>

							</div>

						</div>

						<div class="widget subscribe-widget clearfix">
							<h5><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
							<div class="widget-subscribe-form-result"></div>
							<form id="widget-subscribe-form" action="include/subscribe.php" method="post" class="nobottommargin">
								<div class="input-group divcenter">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="icon-email2"></i></div>
									</div>
									<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
									<div class="input-group-append">
										<button class="btn btn-success" type="submit">Subscribe</button>
									</div>
								</div>
							</form>
						</div>

						<div class="widget clearfix" style="margin-bottom: -20px;">

							<div class="row">

								<div class="col-lg-6 clearfix bottommargin-sm">
									<a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
										<i class="icon-facebook"></i>
										<i class="icon-facebook"></i>
									</a>
									<a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
								</div>
								<div class="col-lg-6 clearfix">
									<a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
										<i class="icon-rss"></i>
										<i class="icon-rss"></i>
									</a>
									<a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
								</div>

							</div>

						</div>

					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2014 All Rights Reserved by Canvas Inc.<br>
						<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
					</div>

					<div class="col_half col_last tright">
						<div class="fright clearfix">
							<a href="#" class="social-icon si-small si-borderless si-facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-gplus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-pinterest">
								<i class="icon-pinterest"></i>
								<i class="icon-pinterest"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-vimeo">
								<i class="icon-vimeo"></i>
								<i class="icon-vimeo"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-github">
								<i class="icon-github"></i>
								<i class="icon-github"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-yahoo">
								<i class="icon-yahoo"></i>
								<i class="icon-yahoo"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-linkedin">
								<i class="icon-linkedin"></i>
								<i class="icon-linkedin"></i>
							</a>
						</div>

						<div class="clear"></div>

						<i class="icon-envelope2"></i> info@canvas.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +91-11-6541-6369 <span class="middot">&middot;</span> <i class="icon-skype2"></i> CanvasOnSkype
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>
	<!-- External JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/jquery.calendario.js"></script>
	<script src="js/events-data.js"></script>
	<script src="js/jquery.gmap.js"></script>
	<script >
		var canvasEvents = {
			<?php
				foreach($eventos as $evento) { 
					$date = new DateTime($evento->fecha);
					$dia = $date->format('d');
					$mes = $date->format('m');
					$hora = $date->format("H:i");
					$anio = $date->format("Y");
			?>
				'<?=$mes?>-<?=$dia?>-<?=$anio?>' : '<a href="detalle_evento.php?evento=<?=$evento->id?>" target=_blank><?=$evento->nombre?></a>',
			<?php 
				} 
			?>
		}
	</script>
	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>
	<script src="js/custom.js"></script>
	<script>
		$('#google-map4').gMap({
			 address: 'Australia',
			 maptype: 'ROADMAP',
			 zoom: 3,
			 markers: [
				{
					address: "Melbourne, Australia",
					html: "Melbourne, Australia"
				},
				{
					address: "Sydney, Australia",
					html: "Sydney, Australia"
				},
				{
					address: "Perth, Australia",
					html: "Perth, Australia"
				}
			 ],
			 doubleclickzoom: false,
			 controls: {
				 panControl: true,
				 zoomControl: true,
				 mapTypeControl: false,
				 scaleControl: false,
				 streetViewControl: false,
				 overviewMapControl: false
			 }
		});
	</script>

</body>
</html>