<?php

include_once "include/modelos/Contacto.php";
date_default_timezone_set("America/Mexico_City");
header('Content-Type: application/json');

$datos = $_POST;

$contacto = new Contacto;
$contacto->nombre = isset($datos["nombre"]) ? $datos["nombre"] : "";
$contacto->email = isset($datos["email"]) ? $datos["email"] : "";
$contacto->telefono = isset($datos["telefono"]) ? $datos["telefono"] : "";
$contacto->mensaje = isset($datos["mensaje"]) ? $datos["mensaje"] : "";

if($contacto->esValido()){
    $contacto->guardar();
    if($contacto->id){
        http_response_code(201);
        echo json_encode(
            [
                "alert" => "success",
                "message" => "Se registró al usuario $contacto->nombre",
                "contacto" => $contacto
            ]
        );
    }
    
    else{
        http_response_code(409);
        echo json_encode(
            [
                "alert" => "error",
                "message" => "No se pudo guardar al contacto intenta de nuevo"
            ]
        );
    }
} else {
    http_response_code(409);
    $response = array(
        "alert" => "error",
        "message" => "Error en la petición"
    );
    echo json_encode($response);
}
?>